
from admin_operations import Admin
ADMIN_OPTIONS = "ENTER 'CREATE' TO CREATE TOPIC\n" \
                "ENTER 'DELETE_TOPIC' TO DELETE A TOPIC\n" \
                "ENTER 'DELETE_MESSAGE' TO MOVE MESSAGE TO DEAD LETTER QUEUE\n"

CREATE_TOPIC = "CREATE"
DELETE_TOPIC = "DELETE_TOPIC"
DELETE_MESSAGE = "DELETE_MESSAGE"

if __name__ == '__main__':
    admin_choice = input(ADMIN_OPTIONS).strip().lower()

    if CREATE_TOPIC.lower() == admin_choice:
        topic_id = Admin().create_topic()
        print("created topic with topic id {}".format(topic_id))

    elif DELETE_TOPIC.lower() == admin_choice:
        Admin().delete_topic()

    elif DELETE_MESSAGE.lower() == admin_choice:
        Admin.delete_message()
        print("MESSAGE IS MOVED TO DEAD LETTER QUEUE")

    else:
        print("INVALID CHOICE")
