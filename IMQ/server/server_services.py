
from settings import *
from model.operations.osub_topic import OSubTopic
from model.entities.esub_topic import ESubTopic
from model.operations.opub_topic import OPubTopic
from model.entities.epub_topic import EPubTopic
from model.operations.otopic import OTopic
from model.entities.emessage import EMessage
from queue_service import QueueService
from exceptions.server_exceptions.exception_class import *
from literals import *


class ServerServices:
    client_id = -1
    topic_id = -1

    def __init__(self, conn):
        self.client_conn = conn

    def connect_client(self, request_obj):
        """connect the client to the topic and initialize the class variable"""
        self.client_id = request_obj['client_id']
        self.topic_id = self.get_topic_id(request_obj['topic_name'])

    def get_last_message_id(self):
        """to get the last read message by subscriber"""
        try:
            sub_topic = OSubTopic().select(self.topic_id, self.client_id)
            if sub_topic == 0:
                return sub_topic
            else:
                last_message_id = sub_topic[0][2]
                return last_message_id
        except IndexError:
            return 0

    def send_message(self):
        """for sending the message to the client"""
        try:
            if self.client_id == -1:
                raise ClientNotConnectedError
            start_from_message_id = self.get_last_message_id()
            message, last_read_message_id = QueueService().get_message(self.topic_id, start_from_message_id)
            if last_read_message_id == -1:
                raise IndexError
            self.save_subscriber(last_read_message_id)
            return message

        except IndexError:
            message = NO_NEW_MSG_IN_TOPIC
            return message

    @staticmethod
    def get_topic_id(topic_name):
        topic = OTopic().select()
        for i in topic:
            if i[1] == topic_name:
                return i[0]
        return 0

    @staticmethod
    def get_all_topic():
        topic = OTopic().select()
        topics = [i[1] for i in topic]
        return topics

    def save_publisher(self):
        """To save the publisher record in DB"""
        epub_topic = EPubTopic(self.client_id, self.topic_id)
        OPubTopic().insert(epub_topic)

    def save_subscriber(self, last_read_message_id):
        """To save the subscriber record in DB"""
        esub_topic = ESubTopic(client_id=self.client_id, topic_id=self.topic_id,
                               last_message=last_read_message_id)
        OSubTopic().insert(esub_topic)

    def save_message(self, message):
        """To save the message in DB"""
        if self.client_id == -1:
            raise ClientNotConnectedError
        self.save_publisher()
        e_message = EMessage(message['data'], message['created_time'],
                             message['expire_time'], message['status'],
                             self.client_id, self.topic_id)
        QueueService().insert_message(e_message)
        print(MESSAGE_SAVED_IN_DB)
        return 0
