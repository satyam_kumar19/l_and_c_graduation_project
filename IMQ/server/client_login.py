
from settings import *
from model.operations.oclient import OClient
from model.entities.eclient import EClient
from exceptions.server_exceptions.exception_class import *


class Login:
    def __init__(self, client_id, client_password):
        self.client_id = int(client_id)
        self.client_password = client_password

    def fetch_client_data(self):
        registered_client = OClient().select()
        for record in registered_client:
            if record[0] == self.client_id:
                return record[0], record[2]
        else:
            raise ClientNotRegisteredError

    def login(self):
        print("login is called")
        client_record = self.fetch_client_data()
        print(client_record)
        print(self.client_password)
        if client_record == -1:
            raise ClientNotRegisteredError
        else:
            if client_record[1] == str(self.client_password):
                return self.client_id
            else:
                raise PasswordNotMatchedError


class Signup:
    def __init__(self, address, password):
        self.client_address = address
        self.client_password = password

    def register(self):
        print("register is called")
        print("client_address = {}".format(self.client_address))
        print("client_password = {}".format(self.client_password))
        client = EClient(self.client_address, self.client_password)
        client_id = OClient().insert(client)
        return client_id
