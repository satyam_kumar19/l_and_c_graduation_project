"""module for server class for client server"""

from settings import *
from threading import *
from constants import BUFFER_SIZE, FORMAT
from protocol.protocol_convertor import ProtocolConverter
from protocol.response_protocol import Response
from exceptions.server_exceptions.exception_class import *
from client_login import Login, Signup
from literals import *
from server_logging import *
from server_services import ServerServices
from protocol.message_queue_protocol import MessageProtocol
from protocol.json_handler import to_json


class Server(Thread):

    def __init__(self, conn, addr):
        Thread.__init__(self)
        self.client_conn = conn
        self.client_addr = addr
        self.server_services = ServerServices(self.client_conn)
        logger.info(CLIENT_INFO_LOG.format(self.client_addr))

    @staticmethod
    def get_byte_message(status_code, status_message, message):
        response = Response(status_message, status_code)
        message = message
        protocol = MessageProtocol(message, response)
        json_obj = to_json(protocol)
        byte_message = ProtocolConverter().json_to_byte(json_obj)
        return byte_message

    def run(self):
        try:
            print(NEW_CONNECTION.format(self.client_addr))
            topics = self.server_services.get_all_topic()
            while True:
                data = self.client_conn.recv(BUFFER_SIZE)
                if EXIT_OPTION == data.decode(FORMAT).lower():
                    break
                obj = ProtocolConverter()
                json_data = obj.byte_to_json(data)
                print(SERVER_RECEIVED, json_data)
                request_type = json_data['request']['request_type']
                print("request_type = " + request_type)

                if CONNECT_OPTION == request_type.lower():
                    self.server_services.connect_client(json_data['request'])
                    topic_name = json_data['request']['topic_name']
                    response = self.get_byte_message(200, "Connected to {}".format(topic_name), {})
                    self.client_conn.send(response)

                elif PULL_OPTION == request_type.lower():
                    message_content = self.server_services.send_message()
                    response = self.get_byte_message(200, "pulled following message:", message_content)
                    self.client_conn.send(response)

                elif LOGIN_OPTION == request_type.lower():
                    client_id = json_data['message_content']['client_id']
                    client_password = json_data['message_content']['client_password']
                    client_id = Login(client_id, client_password).login()
                    message = self.get_byte_message(200, "login successful", AVAILABLE_TOPICS_MSG + str(topics))
                    self.client_conn.send(message)

                elif REGISTER_OPTION == request_type.lower():
                    client_addr = json_data['message_content']['client_address']
                    client_password = json_data['message_content']['client_password']
                    client_id = Signup(client_addr, client_password).register()
                    message_content = AVAILABLE_TOPICS_MSG + str(topics)
                    response = self.get_byte_message(200, "register successful with id = {}".format(client_id),
                                                     message_content)
                    self.client_conn.send(response)

                elif PUSH_OPTION == request_type.lower():
                    self.server_services.save_message(json_data['message_content'])
                    message_content = {}
                    response = self.get_byte_message(200, RECEIVED_MESSAGE, message_content)
                    self.client_conn.send(response)
                logger.info(CLIENT_REQUEST.format(json_data['request']['request_type']))

        except ConnectionResetError:
            self.client_conn.close()
            print(CONNECTION_RESET_MESSAGE)
            logger.error(CONNECTION_RESET_MESSAGE + str(self.client_addr))

        except ConnectionAbortedError:
            print(CONNECTION_RESET_MESSAGE)
            logger.error(CONNECTION_RESET_MESSAGE + str(self.client_addr))
            self.client_conn.close()

        except ClientNotConnectedError as e:
            response = self.get_byte_message(401, str(e), {})
            self.client_conn.send(response)
            self.client_conn.close()
            logger.error(e)

        except ClientNotRegisteredError as e:
            print(e)
            response = self.get_byte_message(404, str(e), {})
            self.client_conn.send(response)
            self.client_conn.close()
            logger.error(e)

        except EOFError as e:
            print(e)
            logger.error(e)

        except PasswordNotMatchedError as e:
            print(e)
            response = self.get_byte_message(401, str(e), {})
            self.client_conn.send(response)
            self.client_conn.close()
            logger.error(e)

        except Exception as e:
            self.client_conn.close()
            print(e)
            logger.error(e)
