"""Module containing all the admin related operations"""
from settings import *
from topic_service import TopicService
from server_logging import logger
from model.operations.dead_letter_queue import DeadLetter
from model.operations.omessage import OMessage


class Admin:
    def __init__(self):
        pass

    @staticmethod
    def create_topic():
        try:
            topic_id = TopicService().create_topic()
            logger.info('Created topic with topic id {}'.format(topic_id))
            return topic_id
        except Exception as e:
            print(e)
            return -1

    @staticmethod
    def delete_topic():
        try:
            deleted_topic = TopicService().delete_topic()
            logger.info('Deleted topic with name {} '.format(deleted_topic))
        except Exception as e:
            print(e)

    @staticmethod
    def delete_message():
        dead_messages = DeadLetter().get_dead_message_id()
        for message_id in dead_messages:
            DeadLetter().insert(message_id)
            OMessage().delete(message_id)
        DeadLetter().update()
