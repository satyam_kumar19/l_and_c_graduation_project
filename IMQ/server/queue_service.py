"""Module for queue related operations"""

from settings import *
from model.operations.omessage import OMessage


class QueueService(object):
    def __init__(self):
        self.o_message = OMessage()

    def insert_message(self, e_message):
        message_id = self.o_message.insert(e_message)
        return message_id

    def delete_message(self, message_id):
        self.o_message.delete(message_id)

    def get_message(self, topic_id, starting_from):
        message = self.o_message.select(topic_id, starting_from)
        if len(message) == 0:
            return [], -1
        message_content = [m[1] for m in message]
        last_read_message_id = max([m[0] for m in message])
        return message_content, last_read_message_id
