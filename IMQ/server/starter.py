import socket
from server import Server
from constants import IP, PORT
from literals import MULTI_SOCKET_BINDING_ERROR, SERVER_LISTENING
ADDR = (IP, PORT)


if __name__ == "__main__":
    try:

        threads = []
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind(ADDR)
        while True:
            server.listen(10)
            print(SERVER_LISTENING.format(IP))
            client_conn, client_addr = server.accept()
            new_thread = Server(client_conn, client_addr)
            new_thread.start()
            threads.append(new_thread)
        for t in threads:
            t.join()
    except OSError:
        print(MULTI_SOCKET_BINDING_ERROR)
