
from settings import *
from model.entities.etopic import ETopic
from model.operations.otopic import OTopic


class TopicService:
    def __init__(self):
        self.topic_op = OTopic()

    def create_topic(self):
        topic_name = self.input_topic_name()
        topic_obj = ETopic(topic_name=topic_name)
        topic_id = self.topic_op.insert(topic_obj)
        return topic_id

    @staticmethod
    def input_topic_name():
        topic_name = input('ENTER TOPIC NAME').strip().lower()
        return topic_name

    def get_all_topic(self):
        topic_record = self.topic_op.select()
        topic_ids = [i[0] for i in topic_record]
        return topic_ids

    def delete_topic(self):
        try:
            topic_name = self.input_topic_name()
            self.topic_op.delete(topic_name)
            print("TOPIC DELETED SUCCESSFULLY")
            return topic_name
        except Exception as e:
            print(e)
            print("Topic is not available")
