RECEIVED_MESSAGE = "MESSAGE IS RECEIVED BY THE QUEUE\n"
DISCONNECT_MESSAGE = "DISCONNECT"
SERVER_LISTENING = "[LISTENING] Server is listening on {}"
MULTI_SOCKET_BINDING_ERROR = 'MULTIPLE SOCKET CAN NOT BE BINDED'
CONNECTION_RESET_MESSAGE = "CONNECTION IS ABORDED BY CLIENT"
CLIENT_INFO_LOG = "client is connected with address = {}\n"
NO_NEW_MSG_IN_TOPIC = "No new message is available in the topic\n"
MESSAGE_SAVED_IN_DB = "Successfully saved in db\n"
AVAILABLE_TOPICS_MSG = "TOPICS AVAILABLE IN THE QUEUE:\n"
CONNECTION_SUCCESS = "Connected to topic\n"
SERVER_RECEIVED = "Server received data:\n"
NEW_CONNECTION = "[NEW CONNECTION] {} connected.\n"
CLIENT_REQUEST = "client is requesting with {} request\n"
PUSH_OPTION = "push"
EXIT_OPTION = "exit"
PULL_OPTION = "pull"
CONNECT_OPTION = "connect"
REGISTER_OPTION = "register"
LOGIN_OPTION = "login"
