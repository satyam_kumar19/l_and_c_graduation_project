"""Request module """


class Request(object):
    """Request protocol utilised by client to make a request"""
    def __init__(self, request_type=0, topic_name=0, client_id=0):
        self.request_type = request_type
        self.topic_name = topic_name
        self.client_id = client_id

    @property
    def request_type(self):
        return self._request_type

    @request_type.setter
    def request_type(self, value):
        self._request_type = value

    @property
    def client_id(self):
        return self._client_id

    @client_id.setter
    def client_id(self, value):
        self._client_id = value

    @property
    def topic_name(self):
        return self._topic_name

    @topic_name.setter
    def topic_name(self, value):
        self._topic_name = value

    def reprJSON(self):
        """Will return the JSON representation of the object properties"""
        return dict(request_type=self.request_type, client_id=self.client_id, topic_name=self.topic_name)
