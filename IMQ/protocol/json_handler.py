"""To handle the different protocol utilised by message protocol; To convert into json format"""
import json


class ComplexEncoder(json.JSONEncoder):
    """To encode the custom object using json serializer; used to get json data using object"""
    def default(self, obj):
        if hasattr(obj, 'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)


def to_json(obj):
    """Method to convert object "obj" to json format"""
    json_ob = json.dumps(obj.reprJSON(), cls=ComplexEncoder)
    return json.loads(json_ob)
