
class Response(object):
    """Response protocol used by server to send the response"""
    def __init__(self, message, status_code):
        self._message = message
        self._status_code = status_code

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, value):
        self._message = value
    
    @property
    def status_code(self):
        return self._status_code

    @status_code.setter
    def status_code(self, value):
        self._status_code = value

    def reprJSON(self):
        """Will return the JSON representation of the object properties"""
        return dict(response_message=self.message, status_code=self.status_code)
