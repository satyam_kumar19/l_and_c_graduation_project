
from constants import IP, PROTOCOL_VERSION


class MessageProtocol(object):
    """Class for message protocol that will use request or response as per requirement"""

    def __init__(self, message, request_type):
        self.protocol_version = PROTOCOL_VERSION
        self.source_add = IP
        self.destination_add = IP
        self.message_content = message
        self.request = request_type
        
    @property
    def protocol_version(self):
        return self._protocol_version

    @protocol_version.setter
    def protocol_version(self, value):
        self._protocol_version = value
    
    @property
    def source_add(self):
        return self._source_add

    @ source_add.setter
    def source_add(self, value):
        self._source_add = value

    @property
    def destination_add(self):
        return self._destination_add

    @destination_add.setter
    def destination_add(self, value):
        self._destination_add = value

    @property
    def message_content(self):
        return self._message_content

    @message_content.setter
    def message_content(self, value):
        self._message_content = value

    @property
    def request(self):
        return self._request

    @request.setter
    def request(self, value):
        self._request = value

    def reprJSON(self):
        """Will return the JSON representation of the object properties"""
        return dict(protocol_version=self.protocol_version,
                    source_add=self.source_add,
                    destination_add=self.destination_add,
                    message_content=self.message_content,
                    request=self.request)
