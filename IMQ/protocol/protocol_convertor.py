"""Module to convert the protocol from json to byte and byte to json"""
import json
from ast import literal_eval


class ProtocolConverter(object):
    def __init__(self):
        pass

    @staticmethod
    def json_to_byte(dict_obj):
        json_data = json.dumps(dict_obj, indent=2)
        byte_data = json_data.encode('utf-8')  # Encode using utf-8 encoding
        return byte_data

    @staticmethod
    def byte_to_json(byte_data):
        decoded_data = literal_eval(byte_data.decode('utf8'))  # Reverse encoding using utf-8
        return decoded_data
