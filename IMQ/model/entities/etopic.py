
class ETopic(object):
    
    def __init__(self, topic_id=0, topic_name=0):
        self.topic_id = topic_id
        self.topic_name = topic_name

    @property
    def topic_id(self):
        return self._topic_id

    @topic_id.setter
    def topic_id(self, value):
        self._topic_id = value

    @property
    def topic_name(self):
        return self._topic_name

    @topic_name.setter
    def topic_name(self, value):
        self._topic_name = value
