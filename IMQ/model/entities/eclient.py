
class EClient(object):
    
    def __init__(self, client_add=0, password=0):
        self.client_address = client_add
        self.client_password = password

    @property
    def client_address(self):
        return self.__client_address

    @client_address.setter
    def client_address(self, value):
        self.__client_address = value

    @property
    def client_password(self):
        return self.__client_password

    @client_password.setter
    def client_password(self, value):
        self.__client_password = value
