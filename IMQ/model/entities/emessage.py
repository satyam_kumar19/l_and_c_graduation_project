
class EMessage(object):
    def __init__(self, data=0, create_time=0, expiry_time=0, status='ACTIVE', client_id=12, topic_id=1):
        self.message_data = data
        self.create_time = create_time
        self.expiry_time = expiry_time
        self.status = status
        self.client_id = client_id
        self.topic_id = topic_id

    @property
    def message_data(self):
        return self._message_data

    @message_data.setter
    def message_data(self, value):
        self._message_data = value
    
    @property
    def create_time(self):
        return self._create_time

    @create_time.setter
    def create_time(self, value):
        self._create_time = value
    
    @property
    def expiry_time(self):
        return self._expiry_time

    @expiry_time.setter
    def expiry_time(self, value):
        self._expiry_time = value

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        self._status = value