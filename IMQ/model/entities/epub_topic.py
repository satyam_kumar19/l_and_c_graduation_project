
class EPubTopic(object):
    def __init__(self, client_id, topic_id):
        self.client_id = client_id
        self.topic_id = topic_id

    @property
    def client_id(self):
        return self._client_id

    @client_id.setter
    def client_id(self, value):
        self._client_id = value

    @property
    def topic_id(self):
        return self._topic_id

    @topic_id.setter
    def topic_id(self, value):
        self._topic_id = value
