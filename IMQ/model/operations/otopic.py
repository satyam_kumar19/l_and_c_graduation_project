
import mysql.connector
from settings import *
from configuration import db_config


class OTopic(object):
    def __init__(self):
        self.my_db = mysql.connector.connect(host=db_config['host'],
                                             user=db_config['user'],
                                             password=db_config['password'],
                                             database=db_config['db_name'])
        self.cursor = self.my_db.cursor()

    # CRUD OPERATIONS
    def insert(self, e_topic):
        query = "INSERT INTO topic (topic_name) VALUES (%s)"
        value = (e_topic.topic_name,)
        self.cursor.execute(query, value)
        self.my_db.commit()
        row_id = self.cursor.lastrowid
        self.my_db.close()
        return row_id

    def update(self):
        pass

    def delete(self, topic_name):
        query = "DELETE FROM topic WHERE topic_name = '{}'".format(topic_name)
        self.cursor.execute(query)
        self.my_db.commit()

    def select(self):
        query = "SELECT * FROM topic"
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        self.my_db.close()
        return result
