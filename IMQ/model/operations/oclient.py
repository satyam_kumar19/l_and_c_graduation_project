
from settings import *
import mysql.connector
from configuration import db_config


class OClient(object):
    def __init__(self):
        self.my_db = mysql.connector.connect(host=db_config['host'],
                                             user=db_config['user'],
                                             password=db_config['password'],
                                             database=db_config['db_name'])
        self.cursor = self.my_db.cursor()

    # CRUD OPERATIONS
    def insert(self, e_client):
        query = "INSERT INTO client (client_address, password) VALUES (%s,%s)"
        value = (e_client.client_address, e_client.client_password)
        self.cursor.execute(query, value)
        self.my_db.commit()
        row_id = self.cursor.lastrowid
        self.my_db.close()
        return row_id

    def update(self):
        pass

    def delete(self, client_id):
        query = "DELETE FROM client WHERE client_id = " + str(client_id)
        self.cursor.execute(query)
        self.my_db.commit()

    def select(self):
        query = "SELECT * FROM client"
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        self.my_db.close()
        return result
