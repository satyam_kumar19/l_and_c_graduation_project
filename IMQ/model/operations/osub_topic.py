from settings import *
import mysql.connector
from configuration import db_config


class OSubTopic(object):
    def __init__(self):
        self.my_db = mysql.connector.connect(host=db_config['host'],
                                             user=db_config['user'],
                                             password=db_config['password'],
                                             database=db_config['db_name'])
        self.cursor = self.my_db.cursor()

    # CRUD OPERATIONS
    def insert(self, e_sub_topic):
        try:
            query = "INSERT INTO sub_topic (topic_id, client_id, last_read_message)" \
                    "VALUES (%s, %s, %s)"
            value = (e_sub_topic.topic_id, e_sub_topic.client_id, e_sub_topic.last_message_id)
            self.cursor.execute(query, value)
            self.my_db.commit()
            row_id = self.cursor.lastrowid
            self.my_db.close()
            return row_id
        except mysql.connector.errors.IntegrityError:
            self.update(e_sub_topic)

    def update(self, e_sub_topic):
        query = "UPDATE sub_topic \
                SET last_read_message = {}\
                WHERE topic_id = {} AND client_id = {}"\
            .format(e_sub_topic.last_message_id, e_sub_topic.topic_id,
                    e_sub_topic.client_id)

        self.cursor.execute(query)
        self.my_db.commit()
        row_id = self.cursor.lastrowid
        self.my_db.close()
        return row_id

    def delete(self):
        pass

    def select(self, topic_id, client_id):
        try:
            query = "SELECT * FROM sub_topic where " \
                    "client_id = "+str(client_id) + \
                    " AND topic_id = " + str(topic_id)
            self.cursor.execute(query)
            result = self.cursor.fetchall()
            self.my_db.close()
            return result
        except Exception:
            return 0
