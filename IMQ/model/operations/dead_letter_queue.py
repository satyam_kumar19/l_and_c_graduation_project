
import mysql.connector
from settings import *
from configuration import db_config
import datetime


class DeadLetter(object):
    def __init__(self):
        self.my_db = mysql.connector.connect(host=db_config['host'],
                                             user=db_config['user'],
                                             password=db_config['password'],
                                             database=db_config['db_name'],
                                             auth_plugin='mysql_native_password')
        self.cursor = self.my_db.cursor()

    # CRUD OPERATIONS
    def insert(self, message_id):
        query = "INSERT dead_letter_queue SELECT * FROM message WHERE message_id = {}".format(message_id)
        self.cursor.execute(query)
        self.my_db.commit()
        row_id = self.cursor.lastrowid
        self.my_db.close()
        return row_id

    def update(self):
        query = "UPDATE dead_letter_queue SET message_status = 'DEAD'"
        self.cursor.execute(query)
        self.my_db.commit()

    def delete(self, message_id):
        query = "DELETE FROM message WHERE message_id = " + str(message_id)
        self.cursor.execute(query)
        self.my_db.commit()

    def get_dead_message_id(self):
        today = datetime.datetime.now()
        print(today)
        query = "SELECT message_id FROM message WHERE expiry_time " \
                " <  %s"
        value = (today, )
        self.cursor.execute(query, value)
        dead_messages = self.cursor.fetchall()
        dead_message_id = [id[0] for id in dead_messages]
        return dead_message_id

    def select(self):
        query = "SELECT * FROM dead_letter"
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        self.my_db.commit()
        self.my_db.close()
        return result
