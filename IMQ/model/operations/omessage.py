
import mysql.connector
from settings import *
from configuration import db_config


class OMessage(object):
    def __init__(self):
        self.my_db = mysql.connector.connect(host=db_config['host'],
                                             user=db_config['user'],
                                             password=db_config['password'],
                                             database=db_config['db_name'],
                                             auth_plugin='mysql_native_password')
        self.cursor = self.my_db.cursor()

    # CRUD OPERATIONS
    def insert(self, e_message):
        query = "INSERT INTO message(message_data, create_time, " \
                "expiry_time, message_status, client_id, topic_id) " \
                "values (%s, %s, %s, %s, %s, %s)"
        value = (e_message.message_data, e_message.create_time,
                 e_message.expiry_time, e_message.status,
                 e_message.client_id, e_message.topic_id)
        self.cursor.execute(query, value)
        self.my_db.commit()
        row_id = self.cursor.lastrowid
        self.my_db.close()
        return row_id

    def update(self):
        pass

    def delete(self, message_id):
        query = "DELETE FROM message WHERE message_id = " + str(message_id)
        self.cursor.execute(query)
        self.my_db.commit()

    def select(self, topic_id, starting_from):
        query = "SELECT * FROM message WHERE TOPIC_ID = {} \
        AND message_id > {}".format(topic_id, starting_from)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        self.my_db.commit()
        self.my_db.close()
        return result
