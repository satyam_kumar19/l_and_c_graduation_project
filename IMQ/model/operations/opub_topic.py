
from settings import *
import mysql.connector
from configuration import db_config


class OPubTopic(object):
    def __init__(self):
        self.my_db = mysql.connector.connect(host=db_config['host'],
                                             user=db_config['user'],
                                             password=db_config['password'],
                                             database=db_config['db_name'])
        self.cursor = self.my_db.cursor()

    # CRUD OPERATIONS
    def insert(self, e_pub_topic):
        try:
            query = "INSERT INTO pub_topic (topic_id, client_id) VALUES (%s, %s)"
            value = (e_pub_topic.topic_id, e_pub_topic.client_id)
            self.cursor.execute(query, value)
            self.my_db.commit()
            row_id = self.cursor.lastrowid
            self.my_db.close()
            return row_id
        except mysql.connector.errors.IntegrityError:
            pass

    def update(self):
        pass

    def delete(self):
        pass

    def select(self):
        query = "SELECT * FROM pub_topic"
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        self.my_db.close()
        return result
