"""Module with client entity"""


class Client:
    def __init__(self, client_add=0,  password=0, client_id=0):
        self.client_address = client_add
        self.client_password = password
        self.client_id = client_id

    @property
    def client_id(self):
        return self.__client_id

    @client_id.setter
    def client_id(self, value):
        self.__client_id = value

    @property
    def client_address(self):
        return self.__client_address

    @client_address.setter
    def client_address(self, value):
        self.__client_address = value

    @property
    def client_password(self):
        return self.__client_password

    @client_password.setter
    def client_password(self, value):
        self.__client_password = value

    def reprJSON(self):
        """Will return the JSON representation of the object properties"""
        return dict(client_id=self.client_id, client_password=self.client_password, client_address=self.client_address)
