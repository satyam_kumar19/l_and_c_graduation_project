"""Module for taking input from client"""

from settings import *
from literals import *
from exceptions.client_exceptions.exception_class import *
import getpass


class ClientInput:
    def __init__(self):
        pass

    def message_input(self):
        try:
            message = input(MESSAGE_INPUT)
            if len(message) < 1:
                raise EmptyMessage
            return message

        except EmptyMessage as e:
            print(e)
            self.message_input()

    def client_id_input(self):
        try:
            client_id = int(input(CLIENT_ID_INPUT_MSG))
            if client_id < 1:
                raise InvalidClientID
            return client_id

        except InvalidClientID as e:
            print(e)
            self.client_id_input()

        except Exception as e:
            print(e)
            self.client_id_input()
    
    def client_address_input(self):
        try:
            client_address = input(CLIENT_ADDRESS_MESSAGE)
            if len(client_address) < 6:
                raise InvalidClientAddressError
            return client_address

        except InvalidClientAddressError as e:
            print(e)
            self.client_address_input()

        except Exception as e:
            print(e)
            self.client_address_input()

    def client_password_input(self):
        try:
            client_password = getpass.getpass(prompt=CLIENT_PASSWORD_MESSAGE)
            if len(client_password) < 6:
                raise InvalidPasswordError
            return client_password

        except InvalidPasswordError as e:
            print(e)
            self.client_password_input()

        except Exception as e:
            print(e)
            self.client_password_input()

    @staticmethod
    def topic_input():
        try:
            topic_name = input(TOPIC_NAME_MESSAGE)
            return topic_name

        except Exception as e:
            print(e)

    @staticmethod
    def req_type_input():
        try:
            req_type = input(REQ_OPTION)
            return req_type

        except Exception as e:
            print(e)

    @staticmethod
    def login_option():
        try:
            option = input(OPTION).strip().lower()
            return option

        except Exception as e:
            print(e)
