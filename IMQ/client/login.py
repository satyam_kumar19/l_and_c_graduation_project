
from settings import *
from protocol.message_queue_protocol import MessageProtocol
from protocol.request_protocol import Request
from protocol.json_handler import to_json
from protocol.protocol_convertor import ProtocolConverter
from client.client import Client
from exceptions.client_exceptions.exception_class import *
from client_input import ClientInput


class Login:
    def __init__(self, client_conn):
        self.client_id = 0
        self.client_password = 0
        self.client_conn = client_conn
    
    def get_message_byte(self): 
        req_obj = Request('login')
        client = Client(client_id=self.client_id, password=self.client_password)
        protocol = MessageProtocol(client, req_obj)
        json_obj = to_json(protocol)
        byte_message = ProtocolConverter().json_to_byte(json_obj)
        return byte_message

    def get_credentials(self):
        client_id = ClientInput().client_id_input()
        first_password = ClientInput().client_password_input()
        second_password = ClientInput().client_password_input()
        if first_password != second_password:
            raise PasswordNotMatchedError
        else:
            self.client_id = client_id
            self.client_password = first_password

    def login(self):
        self.get_credentials()
        byte_message = self.get_message_byte()
        self.client_conn.send(byte_message)
        message = self.client_conn.receive()
        return message, self.client_id
