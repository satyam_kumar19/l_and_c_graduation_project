"""This module is used to generate byte message with connect request"""

from settings import *
from protocol.message_queue_protocol import MessageProtocol
from protocol.request_protocol import Request
from protocol.json_handler import to_json
from protocol.protocol_convertor import ProtocolConverter


def get_connect_message(topic_name, client_id):
    req_obj = Request('Connect', topic_name, client_id)
    message = {}
    protocol = MessageProtocol(message, req_obj)
    json_obj = to_json(protocol)
    byte_message = ProtocolConverter().json_to_byte(json_obj)
    return byte_message
