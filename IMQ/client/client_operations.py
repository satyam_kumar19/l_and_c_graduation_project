"""To handle the client related operations """

import ast
import json
from client_logging import *
from constants import PORT, IP
from generate_connect_message import get_connect_message
from literals import *
from publisher import Publisher
from subscriber import Subscriber
from constants import FORMAT
from client_input import ClientInput
from exceptions.client_exceptions.exception_class import *


def client_communication(client, client_id, topics):
    topic_name = ClientInput().topic_input()
    if topic_name not in topics:
        raise InvalidTopicNameError
    byte_message = get_connect_message(topic_name, client_id)
    client.send(byte_message)
    response = client.receive()
    display_response_messages(json.loads(response))
    
    while True:
        req_type = ClientInput().req_type_input()
        input_msg = ''

        if PUSH_OPTION == req_type:
            input_msg = ClientInput().message_input()
            Publisher(client_id, client).push_message(input_msg, topic_name)
            ack = client.receive()
            display_response_messages(json.loads(ack))
            logger.info(PUSHED_MESSAGE)

        elif PULL_OPTION == req_type:
            message = Subscriber(client_id, client).pull_message(topic_name)
            display_response_messages(json.loads(message))
            logger.info(RECEIVED_MESSAGE)

        elif EXIT_MSG == req_type:
            client.send(bytes(EXIT_MSG, FORMAT))
            client.exit()
            logger.info(DISCONNECT_MESSAGE.format((IP, PORT)))

        else:
            raise InvalidOptionError


def get_available_topics(message):
    topics = ast.literal_eval(message['message_content'][31:])
    return topics


def display_response_messages(json_message):
    message_content = json_message['message_content']
    response_status = json_message['request']['response_message']
    print("response_status_message: {}".format(response_status))
    if message_content:
        print(message_content)
