"""Module to handle publisher operation like pushing message"""

from settings import *
from protocol.message_queue_protocol import MessageProtocol
from protocol.request_protocol import Request
from protocol.json_handler import to_json
from protocol.protocol_convertor import ProtocolConverter
from message_factory.message import Message


class Publisher(object):
    def __init__(self, client_id, client_conn):
        self.client_id = client_id
        self.client = client_conn

    def subscribe(self):
        pass

    def get_message_byte(self, message_data, topic_name):
        req_obj = Request('PUSH', topic_name, self.client_id)
        message = Message(message_data)
        protocol = MessageProtocol(message, req_obj)
        json_obj = to_json(protocol)
        byte_message = ProtocolConverter().json_to_byte(json_obj)
        return byte_message

    def push_message(self, message, topic_name):
        byte_message = self.get_message_byte(message, topic_name)
        self.client.send(byte_message)
        return 0
