"""starter class for client"""


import json
from client_logging import *
from settings import *
from client_controller import ClientController
from literals import *
from client_input import ClientInput
from exceptions.client_exceptions.exception_class import *
from login import Login
from signup import Register
from client_operations import client_communication, get_available_topics, display_response_messages

if __name__ == "__main__":
    try:
        print(WELCOME_MESSAGE)
        client = ClientController()
        client.connect()
        option = ClientInput().login_option()
        if option == LOGIN_OPTION:
            json_data, client_id = Login(client).login()
            data = json.loads(json_data)
            response_status = data['request']['status_code']
            if response_status == 200:
                topics = get_available_topics(data)
                display_response_messages(data)
                client_communication(client, client_id, topics)

            if response_status == 404:
                raise ClientNotRegisteredError

            else:
                raise PasswordNotMatchedError

        elif option == REGISTER_OPTION:
            message = Register(client).register()
            json_message = json.loads(message)
            response_status = json_message['request']['status_code']
            if response_status == 200:
                topics = get_available_topics(json_message)
                display_response_messages(json_message)
                client_id = int(''.join(filter(str.isdigit, json_message['request']['response_message'])))
                client_communication(client, client_id, topics)

            if response_status == 404:
                raise ClientNotRegisteredError

            else:
                raise PasswordNotMatchedError

        else:
            raise InvalidOptionError

    except InvalidOptionError:
        print(INVALID_OPTION_MESSAGE)
        logger.error(INVALID_OPTION_MESSAGE)

    except ConnectionRefusedError:
        print(CONNECTION_REFUSED_MESSAGE)
        logger.error(CONNECTION_REFUSED_MESSAGE)

    except ConnectionResetError:
        print(CONNECTION_RESET)
        logger.error(CONNECTION_RESET)
    
    except PasswordNotMatchedError as e:
        print(e)
        logger.error(e)
    
    except InvalidTopicNameError as e:
        print(e)
        logger.error(e)

    except ClientNotRegisteredError as e:
        print(e)
        logger.error(e)
