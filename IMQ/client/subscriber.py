"""Module to handle subscriber operation like pulling message"""

from settings import *
from protocol.message_queue_protocol import MessageProtocol
from protocol.request_protocol import Request
from protocol.json_handler import to_json
from protocol.protocol_convertor import ProtocolConverter


class Subscriber(object):
    def __init__(self, client_id, client_conn):
        self.client_id = client_id
        self.client_conn = client_conn

    def get_byte_message(self, topic_name):
        req_obj = Request('PULL', topic_name, self.client_id)
        message = {}
        protocol = MessageProtocol(message, req_obj)
        json_obj = to_json(protocol)
        byte_message = ProtocolConverter().json_to_byte(json_obj)
        return byte_message

    def pull_message(self, topic_name):
        byte_message = self.get_byte_message(topic_name)
        self.client_conn.send(byte_message)
        data = self.client_conn.receive()
        return data

    def subscribe(self):
        pass
