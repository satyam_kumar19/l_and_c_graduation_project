"""Logging module for client"""
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s:%(name)s:%(message)s')
file_handler = logging.FileHandler('client.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
