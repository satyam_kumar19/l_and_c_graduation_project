"""controller for client related communication"""
import socket
import sys
from constants import FORMAT
from literals import DISCONNECT_MESSAGE
from constants import PORT, IP


class ClientController:
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ip = IP
        self.port = PORT

    def connect(self):
        addr = (self.ip, self.port)
        try:
            self.client.connect(addr)
        except ConnectionRefusedError:
            raise ConnectionRefusedError

    def receive(self):
        data = self.client.recv(1024).decode(FORMAT)
        return data

    def send(self, msg):
        self.client.send(msg)

    def exit(self, host=''):
        host = (self.ip, self.port)
        print(DISCONNECT_MESSAGE.format(host))
        sys.exit()
