"""Exception class for server"""

from exceptions.exception_literals import *


class CustomServerError(Exception):
    "Base custom class for server"
    def __init__(self):
        self.message = CUSTOM_SERVER_ERROR

    def __str__(self):
        return self.message


class ClientNotConnectedError(CustomServerError):
    """Exception class if client is not connected"""

    def __init__(self):
        self.message = CLIENT_NOT_CONNECTED_ERROR

    def __str__(self):
        return self.message


class PasswordNotMatchedError(CustomServerError):
    """Exception class if password doesn't match"""

    def __init__(self):
        self.message = PASSWORD_NOT_MATCHED_MESSAGE

    def __str__(self):
        return self.message


class ClientNotRegisteredError(CustomServerError):
    """Exception class if client is not registered"""

    def __init__(self):
        self.message = CLIENT_NOT_REGISTERED_MESSAGE

    def __str__(self):
        return self.message
