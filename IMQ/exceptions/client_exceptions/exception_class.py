"""Exception class for client application"""

from exceptions.exception_literals import *


class CustomClientError(Exception):
    """Base custom class for client application"""
    def __init__(self):
        self.message = CUSTOM_CLIENT_ERROR

    def __str__(self):
        return self.message


class EmptyMessage(CustomClientError):
    """custom class for empty null message"""
    def __init__(self):
        self.message = NULL_MESSAGE_ERROR

    def __str__(self):
        return self.message


class InvalidPasswordError(CustomClientError):
    """class invalid client password for client application"""
    def __init__(self):
        self.message = INVALID_PASSWORD_MESSAGE

    def __str__(self):
        return self.message


class InvalidClientID(CustomClientError):
    """class invalid client id for client application"""
    def __init__(self):
        self.message = INVALID_CLIENT_ID_MESSAGE

    def __str__(self):
        return self.message


class InvalidClientAddressError(CustomClientError):
    """Invalid client address class for client application"""
    def __init__(self):
        self.message = INVALID_ADDRESS_MESSAGE

    def __str__(self):
        return self.message


class InvalidOptionError(CustomClientError):
    """Invalid option class for client application"""
    def __init__(self):
        self.message = INVALID_OPTION_MESSAGE

    def __str__(self):
        return self.message


class PasswordNotMatchedError(CustomClientError):
    """Exception class when password doesn't match"""

    def __init__(self):
        self.message = PASSWORD_NOT_MATCHED_MESSAGE

    def __str__(self):
        return self.message


class InvalidTopicNameError(CustomClientError):
    """custom class for empty null message"""
    def __init__(self):
        self.message = INVALID_TOPIC_INPUT

    def __str__(self):
        return self.message

class ClientNotRegisteredError(CustomClientError):
    """Exception class if client is not registered"""

    def __init__(self):
        self.message = CLIENT_NOT_REGISTERED_MESSAGE

    def __str__(self):
        return self.message