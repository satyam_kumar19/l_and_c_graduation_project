"""Factory module for message generation"""

from message_factory.json_factory import JSON


class MessageFactory(object):
    def __init__(self):
        pass

    @staticmethod
    def create_message(message_data, message_type):
        try:
            if message_type == 'json':
                return JSON.CreateMessage(message_data)

            raise AssertionError("type of object not found")
        except AssertionError as e:
            print(e)
