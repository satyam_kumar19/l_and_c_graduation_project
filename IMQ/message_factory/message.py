"""Module for IMQ messages"""

import datetime


class Message(object):
    def __init__(self, data):
        self.created_time = str(datetime.datetime.now())
        self.expire_time = str(datetime.date.today() + datetime.timedelta(days=1))
        self.data = data
        self.status = "ACTIVE"

    @property
    def created_time(self):
        return self._created_time

    @created_time.setter
    def created_time(self, value):
        self._created_time = value

    @property
    def expire_time(self):
        return self._expire_time

    @expire_time.setter
    def expire_time(self, value):
        self._expire_time = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        self._status = value

    def reprJSON(self):
        """Will return the JSON representation of the object properties"""
        return dict(created_time=self.created_time, expire_time=self._expire_time,
                    data=self.data, status=self.status)
