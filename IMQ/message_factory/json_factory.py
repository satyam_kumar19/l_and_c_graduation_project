"""Factory for getting json message"""
import json


class JSON:
    def __init__(self):
        pass

    @staticmethod
    def create_message(message_obj):
        """Method to convert object "obj" to json format"""
        json_obj = json.dumps(message_obj.__dict__)
        return json.loads(json_obj)
