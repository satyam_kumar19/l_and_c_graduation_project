#InTimeTec Messaging Queue (IMQ)#

### Purpose of this Project ###

This project is to implement the messaging queue system.

### Project Description ###

Messaging queue is a form of the asynchronous communication process in which, basically two types of users are there:
1. Publisher: Publisher is the user who is responsible for pushing the message
   in the queue for the subscriber to consume that message.
2. Subscriber: Subscriber is the user who is responsible for taking the message from the queue and consuming it.

### Assumptions ###
Messages are stored in a queue and it is consumed by subscriber. 
One message will be consumed by many subscriber before the expiry time of the message.
At once one client can be connect to only one topic while he may register to multiple client.
